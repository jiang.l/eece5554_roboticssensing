clear all
statdata = rosbag('station.bag');
movdata = rosbag('move.bag');
stattopic = select(statdata,'Topic','/data');
movtopic = select(movdata,'Topic','/data');
station = readMessages(stattopic,'dataformat','struct');
move = readMessages(movtopic,'dataformat','struct');
latitude(:,1)= cellfun(@(m) double(m.Easting), station);
longitude(:,1)= cellfun(@(m) double(m.Northing), station);
altitude(:,1)=cellfun(@(m) double(m.Altitude), station);
latitude2= cellfun(@(m) double(m.Easting), move);
longitude2= cellfun(@(m) double(m.Northing), move);
altitude2=cellfun(@(m) double(m.Altitude), move);
M_lat= mean(latitude);
M_lon=mean(longitude);
varlat = var(latitude);
varlon=var(longitude);

p=polyfit(latitude2,longitude2,1);
x1=linspace(min(latitude2),max(latitude2));
y1=polyval(p,x1);
yfit=polyval(p,latitude2)
error = longitude2-yfit;
mse=mean(error.^2)

figure(1)
plot3(latitude(:,1),longitude(:,1),altitude(:,1),'r','linewidth',2,'markersize',10)
ylabel('longitude')
xlabel('latitude')
zlabel('altitude')

figure(2)
plot(latitude(:,1),longitude(:,1), 'o')
hold on;
plot(M_lat,M_lon, 'X')
ylabel('longitude')
xlabel('latitude')

figure(3)
plot3(latitude2,longitude2,altitude2,'r','linewidth',2,'markersize',10)
ylabel('longitude')
xlabel('latitude')
zlabel('altitude')

figure(4)

plot(latitude2,longitude2, 'o')
hold on;
plot(x1,y1, '-')
ylabel('longitude')
xlabel('latitude')

figure(5)
plot(latitude2, error)