#!/usr/bin/env python 
# -*- coding: utf-8 -*- 

import rospy 
import serial 
import utm
from std_msgs.msg import Time,UInt32,String
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField
from geometry_msgs.msg import Quaternion
from transformations import quaternion_from_euler
import math

if __name__ == '__main__': 
	rospy.init_node('imu_driver_node', anonymous=True)
	serial_port = rospy.get_param('~port','/dev/ttyUSB0') 
    	serial_baud = rospy.get_param('~baudrate',115200) 
    	sampling_rate = rospy.get_param('~sampling_rate',40) 
    	port = serial.Serial(serial_port, serial_baud, timeout=3.) 
	pub = rospy.Publisher('data',Imu, queue_size=5)
	pub2 = rospy.Publisher('data2',MagneticField, queue_size=5)
	rospy.sleep(0.2)
	msg = Imu()
	msg2 = MagneticField()
	msg.header.frame_id = "CNM"
	msg.header.seq=0
	msg2.header.frame_id = "CNM"
	msg2.header.seq=0
	sleep_time = 1/sampling_rate - 0.025
	rospy.loginfo(0)
	try:
        	while not rospy.is_shutdown():
            		line = port.readline()
		    	if line == '':
				rospy.loginfo(1)
		        	rospy.logwarn("No data")
		    	else:
				rospy.loginfo(2)
 		        	if line.find('$VNYMR')!=-1:
					rospy.loginfo(3)
					msg.header.stamp=rospy.Time.now()
					msg2.header.stamp=rospy.Time.now()
                    			data = line.split(",")
                    			yaw = data[1]
                    			pitch = data[2]
                    			roll = data[3]
					megx = data[4]
					megy = data[5]
					megz = data[6]
					Accelx = data[7]
					Accely = data[8]
					Accelz = data[9]
					Gyrox = data[10]
					Gyroy = data[11]
					rospy.loginfo(data[12])
					if data[12].index('*') == -1:
						Gyroz = data[12]
					else:
						Gyroz = data[12][:data[12].index('*')]	
					rospy.loginfo(float(Gyroz))				
					#if yaw.strip()==''|pitch.strip()==''|roll.strip()=='':
					#	continue 
					rospy.loginfo(float(yaw))
					rospy.loginfo(float(pitch))
					rospy.loginfo(float(roll))
					msg.header.seq += 0
					msg2.header.seq += 0
					q = quaternion_from_euler(float(roll),float(pitch),float(yaw))
					msg.orientation = Quaternion(q[0],q[1],q[2],q[3])
					msg.linear_acceleration.x=float(Accelx)* 9.7887/1000.0
					msg.linear_acceleration.y=float(Accely)* 9.7887/1000.0
					msg.linear_acceleration.z=float(Accelz)* 9.7887/1000.0
					msg.angular_velocity.x = float(Gyrox)*math.pi/10.0/180.0
					msg.angular_velocity.y = float(Gyroy)*math.pi/10.0/180.0
					msg.angular_velocity.z = float(Gyroz)*math.pi/10.0/180.0

					msg2.magnetic_field.x = float(megx)
					msg2.magnetic_field.y = float(megy)
					msg2.magnetic_field.z = float(megz)
					pub.publish(msg)
					pub2.publish(msg2)
			rospy.sleep(sleep_time)
                			
	except rospy.ROSInterruptException: 
        	port.close() 
