#!/usr/bin/env python 
# -*- coding: utf-8 -*- 

import rospy 
import serial 
import utm
from std_msgs.msg import Time,UInt32,String
from device_driver.msg import device_driverX

if __name__ == '__main__': 
	rospy.init_node('device_driver_node', anonymous=True)
	serial_port = rospy.get_param('~port','/dev/ttyUSB2') 
    	serial_baud = rospy.get_param('~baudrate',4800) 
    	sampling_rate = rospy.get_param('~sampling_rate',5.0) 
    	port = serial.Serial(serial_port, serial_baud, timeout=3.) 
	pub = rospy.Publisher('data',device_driverX, queue_size=5)
	rospy.sleep(0.2)
	msg = device_driverX()
	msg.header.frame_id = "CNM"
	msg.header.seq=0
	sleep_time = 1/sampling_rate - 0.025
	try:
        	while not rospy.is_shutdown():
            		line = port.readline()
		    	if line == '':
		        	rospy.logwarn("No data")
		    	else:
 		        	if line.startswith('$GPGGA'):
					msg.header.stamp=rospy.Time.now()
                    			data = line.split(",")
                    			lat = data[2]
                    			lon = data[4]
                    			alt = data[9]
					if lat.strip()=='':
						continue 
					if lon.strip()=='':
						continue
					if alt.strip()=='':
						continue
					rospy.loginfo(lat)
					rospy.loginfo(lon)
					rospy.loginfo(alt)
					msg.header.seq += 0
                    			msg.latitude = (int(float(lat)/100) + (float(lat) % 100)/60)
                    			msg.longitude = -(int(float(lon)/100) + (float(lon) % 100)/60)
					msg.altitude = float(alt)
                    			utm_con = utm.from_latlon(msg.latitude, msg.longitude)
                			msg.easting = utm_con[0]
               		 		msg.northing = utm_con[1]
                			msg.zone = utm_con[2]
               		  		msg.letter = utm_con[3]
					pub.publish(msg)
			rospy.sleep(sleep_time)
                			
	except rospy.ROSInterruptException: 
        	port.close() 
