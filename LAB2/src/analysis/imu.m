clear all
statdata = rosbag('imu.bag');
topic = select(statdata,'Topic','/data');
megtopic = select(statdata,'Topic','/data2');
imu = readMessages(topic,'dataformat','struct');
meg = readMessages(megtopic,'dataformat','struct');
%b = struct2cell(structfun(@(x)x.phase,data,'un',0));
Orientation(:,1)= cellfun(@(m) double(m.Orientation.X), imu);
Orientation(:,2)= cellfun(@(m) double(m.Orientation.Y), imu);
Orientation(:,3)= cellfun(@(m) double(m.Orientation.Z), imu);
Orientation(:,4)= cellfun(@(m) double(m.Orientation.W), imu);
AngularVelocity(:,1)= cellfun(@(m) double(m.AngularVelocity.X), imu);
AngularVelocity(:,2)= cellfun(@(m) double(m.AngularVelocity.Y), imu);
AngularVelocity(:,3)= cellfun(@(m) double(m.AngularVelocity.Z), imu);
LinearAcceleration(:,1)= cellfun(@(m) double(m.LinearAcceleration.X), imu);
LinearAcceleration(:,2)= cellfun(@(m) double(m.LinearAcceleration.Y), imu);
LinearAcceleration(:,3)= cellfun(@(m) double(m.LinearAcceleration.Z), imu);

MagneticField(:,1)= cellfun(@(m) double(m.MagneticField.X), meg);
MagneticField(:,2)= cellfun(@(m) double(m.MagneticField.Y), meg);
MagneticField(:,3)=cellfun(@(m) double(m.MagneticField.Z), meg);

mean_angularvelocity=mean(AngularVelocity)
std_AngularVelocity=std(AngularVelocity)
mean_LinearAcceleration=mean(LinearAcceleration)
std_LinearAcceleration=std(LinearAcceleration)
mean_MagneticField=mean(MagneticField)
std_MagneticField=std(MagneticField)
%M_lat= mean(Orientation);
%M_lon=mean(longitude);
%varlat = var(Orientation);
%varlon=var(longitude);

%p=polyfit(latitude2,longitude2,1);
%x1=linspace(min(latitude2),max(latitude2));
%y1=polyval(p,x1);
%yfit=polyval(p,latitude2)
%error = longitude2-yfit;
%mse=mean(error.^2)


figure(2)
scatter3(AngularVelocity(:,1),AngularVelocity(:,2),AngularVelocity(:,3), 'MarkerEdgeColor','k','MarkerFaceColor',[.75 .75 .75])
title('AngularVelocity')
xlabel('x')
ylabel('y')
zlabel('z')

figure(3)
scatter3(LinearAcceleration(:,1),LinearAcceleration(:,2),LinearAcceleration(:,3),'MarkerEdgeColor','k','MarkerFaceColor',[.75 0 .75])
title('LinearAcceleration')
xlabel('x')
ylabel('y')
zlabel('z')

figure(4)
scatter3(MagneticField(:,1),MagneticField(:,2),MagneticField(:,3),'MarkerEdgeColor','k','MarkerFaceColor',[.75 .75 0])
title('MagneticField')
xlabel('x')
ylabel('y')
zlabel('z')
