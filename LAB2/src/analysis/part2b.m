clear all
statdata = rosbag('car_imu.bag');
gpsdata = rosbag('car_gnss.bag');
 topic = select(statdata,'Topic','/IMU/ypr');
megtopic = select(statdata,'Topic','/IMU/mag');
cleantopic = select(statdata,'Topic','/IMU/clean');
gpstopic=select(gpsdata,'Topic','/GNSS/clean');
 ypr = readMessages(topic,'dataformat','struct');
 imu = readMessages(cleantopic,'dataformat','struct');
meg = readMessages(megtopic,'dataformat','struct');
gps1=readMessages(gpstopic,'dataformat','struct');
starttime=statdata.StartTime;
endtime=statdata.EndTime;
figure(10)

east= cellfun(@(m) double(m.UtmEasting), gps1);
north= cellfun(@(m) double(m.UtmNorthing), gps1);
for n = 2:length(east)
    velocitygps(n-1,1)= sqrt((east(n,1)-east(n-1,1))^2+(north(n,1)-north(n-1,1))^2);
end
plot(east,north,'.');
xlabel('east');
ylabel('north');
title('East vs. North data from gps')

AngularVelocity(:,1)= cellfun(@(m) double(m.AngularVelocity.X), imu);
AngularVelocity(:,2)= cellfun(@(m) double(m.AngularVelocity.Y), imu);
AngularVelocity(:,3)= cellfun(@(m) double(m.AngularVelocity.Z), imu);
LinearAcceleration(:,1)= cellfun(@(m) double(m.LinearAcceleration.X), imu);
LinearAcceleration(:,2)= cellfun(@(m) double(m.LinearAcceleration.Y), imu);
LinearAcceleration(:,3)= cellfun(@(m) double(m.LinearAcceleration.Z), imu);

AngularVelocity(end,:) = [];
LinearAcceleration(end,:)=[];
MagneticField(:,1)= cellfun(@(m) double(m.MagneticField.X), meg);
MagneticField(:,2)= cellfun(@(m) double(m.MagneticField.Y), meg);
MagneticField(:,3)= cellfun(@(m) double(m.MagneticField.Z), meg);
% n=length(MagneticField(:,1));
% MagneticField(:,3)=zeros(n,1);
yaw= cellfun(@(m) double(m.Vector.X), ypr);
pitch= cellfun(@(m) double(m.Vector.Y), ypr);
roll=cellfun(@(m) double(m.Vector.Z), ypr);

sec= cellfun(@(m) double(m.Header.Stamp.Sec), imu);
nsec= cellfun(@(m) double(m.Header.Stamp.Nsec), imu)/10^9;
time = linspace(0,endtime-starttime,length(yaw))';
secmag= cellfun(@(m) double(m.Header.Stamp.Sec), meg);
nsecmag= cellfun(@(m) double(m.Header.Stamp.Nsec), meg)/10^9;
timemag = linspace(0,endtime-starttime,length(yaw))';

velocity(:,1) = cumtrapz(time,LinearAcceleration(:,1));
velocity(:,2) = cumtrapz(time,LinearAcceleration(:,2));
velocity(:,3) = cumtrapz(time,LinearAcceleration(:,3));
% figure(7)
% plot(time, velocity(:,3));
% figure(5)
% plot(time, velocity(:,1));
% figure(6)
% plot(time, velocity(:,2));

% mean_angularvelocity=mean(AngularVelocity)
% std_AngularVelocity=std(AngularVelocity)
% mean_LinearAcceleration=mean(LinearAcceleration)
% std_LinearAcceleration=std(LinearAcceleration)
mean_MagneticField=mean(MagneticField)
std_MagneticField=std(MagneticField)


MagneticField = MagneticField./repmat(sqrt(sum(MagneticField.^2,1)),size(MagneticField,1),1);
ellipse(:,1)=MagneticField(800:2400,1);
ellipse(:,2)=MagneticField(800:2400,2);
[z,a,b,alp]=fitellipse(ellipse,'linear');
ellipse(:,1)=ellipse(:,1)-z(1,1);
ellipse(:,2)=ellipse(:,2)-z(2,1);
ellipsez(:,1)=sin(alp)*ellipse(:,1)-cos(alp)*ellipse(:,2);
ellipsez(:,2)=cos(alp)*ellipse(:,1)+sin(alp)*ellipse(:,2);
xcal(:,1)=ellipsez(:,1)*sqrt(a/b);
xcal(:,2)=ellipsez(:,2)*sqrt(b/a);
wholetrip(:,1)=MagneticField(:,1)-z(1,1);
wholetrip(:,2)=MagneticField(:,2)-z(2,1);
wholetripz(:,1)=sin(alp)*wholetrip(:,1)-cos(alp)*wholetrip(:,2);
wholetripz(:,2)=cos(alp)*wholetrip(:,1)+sin(alp)*wholetrip(:,2);
xcalwhole(:,1)=wholetripz(:,1)*sqrt(a/b);
xcalwhole(:,2)=wholetripz(:,2)*sqrt(b/a);

%LinearAcceleration
%according to gps, car stopped at 132-179,321-355 632-663
velocity(:,1) = cumtrapz(time,LinearAcceleration(:,1));
%there are three stop during the whole ride 
temp = LinearAcceleration(:,1);
delta0S= velocity(133*40,1);
for n = 1 :133*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta0S/130;
end
delta0M=velocity(81*40,1)-velocity(26*40,1);
% for n = 26*40 :81*40
%     LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta0M/(81-26);
% end

%stop1
delta1S= velocity(179*40,1)-velocity(132*40,1);
for n = 132*40 :179*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta1S/(179-132);
end
delta1M=velocity(321*40,1)-velocity(179*40,1);
for n = 179*40 :321*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta1M/(321-179);
end
%stop2
delta2S= velocity(355*40,1)-velocity(321*40,1);
for n = 321*40 :355*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta2S/(355-321);
end
delta2M=velocity(406*40,1)-velocity(355*40,1);
for n = 355*40 :406*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta2M/(406-355);
end
%stop3
delta3M=velocity(437*40,1)-velocity(406*40,1);
for n = 406*40 :437*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta3M/(437-406);
end
delta4M=velocity(477*40,1)-velocity(437*40,1);
for n = 437*40 :477*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta4M/(477-437);
end
delta5M=velocity(520*40,1)-velocity(477*40,1);
for n = 477*40 :520*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta5M/(520-477);
end
delta6M=velocity(590*40,1)-velocity(520*40,1);
for n = 520*40 :590*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta6M/(590-520);
end
delta7M=velocity(632*40,1)-velocity(590*40,1);
for n = 590*40 :632*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta7M/(632-590);
end
%stop4
delta4= velocity(663*40,1)-velocity(632*40,1);
for n = 632*40 :663*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta4/(663-632);
end
delta8M=velocity(737*40,1)-velocity(663*40,1);
for n = 669*40 :737*40
    LinearAcceleration(n,1) = LinearAcceleration(n,1)-delta8M/(737-663);
end
velocitycal(:,1) = cumtrapz(time,LinearAcceleration(:,1));
for n = 1:length(velocitycal)
    if velocitycal(n,1)<0
        velocitycal(n,1)=0;
    end 
end
figure(7)
plot([1:length(velocitygps)], velocitygps);hold on;
plot(time, velocity(:,1)); 
plot(time,velocitycal);hold on;
xlabel('time')
ylabel('forward velocity')
legend('forward velocity calculated by east and north', 'forward velocity from integral of forward acceleration','calibrated forward velocity')
title('time vs.forward velocity')
grid on;
hold off;

[zc,ac,bc,alpc]=fitellipse(xcal(:,1:2),'linear');
% Mag = MagneticField(2800:5000,:);
yawest=atan2(-xcal(:,2),xcal(:,1));
yawest = unwrap(yawest);
yawestwhole=atan2(-xcalwhole(:,2),xcalwhole(:,1));
yawestwhole=unwrap(yawestwhole);
theta_gyroz = cumtrapz(time,AngularVelocity(:,3));
% filter_gyroz=highpass(theta_gyroz,40,1000);
% filter_yawest=lowpass(yawest,10000,1000);
% comp_filter=0.98*filter_gyroz(2800:5000,1)+0.02*filter_yawest;

fuse = complementaryFilter('SampleRate',40);
zzz=zeros(length(xcalwhole(:,1)),1);
[filter_quat, filter_angularvel]=fuse(LinearAcceleration,AngularVelocity,[xcalwhole,zzz]);
compfilter = quat2eul(filter_quat);
figure(15)
% plot(time(2800:5000,1),filter_gyroz(2800:5000,1),'r'),hold on;
yaw1=yaw*pi/180;
compfilter= compfilter(:,1);
plot(timemag(800:2400,1),-1*compfilter(800:2400,1),'b'), hold on,
plot(timemag(800:2400,1),yaw1(800:2400,1),'g'), hold on,
hold off;
figure(20)
compfilter1=unwrap(compfilter);
yaw1= unwrap(yaw*pi/180);
plot(timemag(800:2400,1), yawestwhole(800:2400,1)), hold on;
plot(timemag(800:2400,1),-1*compfilter1(800:2400,1)), hold on,
plot(timemag(800:2400,1),yaw1(800:2400,1)), hold on,
plot(timemag(800:2400,1),theta_gyroz(800:2400,1));
xlabel('time')
ylabel('yaw angle')
legend('yaw calculated by corrected mag', 'yaw integrated from gyroz','yaw from imu reading','yaw from comp filter')
title('time vs. unwrapped Yaw at the beginning circle')
hold off;
%whole trip

filter_gyroz=highpass(theta_gyroz,40,1000);
filter_yawestwhole=lowpass(yawestwhole,40,1000);
comp_filterwhole=0.98*theta_gyroz+0.02*filter_yawestwhole;
comp_filterwhole1=unwrap(comp_filterwhole);
figure(21)
plot(time, yawestwhole), hold on;
plot(time,-1*compfilter1), hold on,
plot(time,yaw1), hold on,
plot(time,theta_gyroz);
xlabel('time')
ylabel('yaw angle')
legend('yaw calculated by corrected mag', 'yaw integrated from gyroz','yaw from imu reading','yaw from comp filter')
title('time vs. unwrapped Yaw')
hold off;

figure(4)
plot(MagneticField(400:2400,1),MagneticField(400:2400,2),'.'), hold on;
plot(xcal(:,1),xcal(:,2),'o'), hold on;
plotellipse(z, a, b, alp, 'k'), hold on;
plotellipse(zc, ac, bc, alpc, 'k');
axis([-0.008 0.008 -0.008 0.008]),
axis equal,
grid on,
title('MagneticField x vs. y')
xlabel('x')
ylabel('y')
legend('Magnetometer data before correction', 'Magnetometer data after correction')
figure (11)
plot(timemag(800:2400,1),MagneticField(800:2400,1))
figure (12)
plot(timemag(800:2400,1),MagneticField(800:2400,1))

%dead reckoning
yaw_y= -cos(yaw1);
yaw_x= sin(yaw1);
velocity_x=yaw_x.*velocitycal;
velocity_y=yaw_y.*velocitycal;
north_est=cumtrapz(time,velocity_x);
east_est=cumtrapz(time,velocity_y);
y_est =  AngularVelocity(:,3).*velocity_x;
figure (30)
plot(time,y_est,'r');hold on;
plot(time, LinearAcceleration(:,2),'b');hold on;
plot(time,temp,'y');
title('y_obs vs. wX')
xlabel('time')
ylabel('acceleration')
legend('wX', 'corrected LinearAcceleration_y','uncorrected LinearAcceleration')
hold off;
north(:,1) = north(:,1)-north(1,1);
east(:,1) = east(:,1)-east(1,1);

velocity_calx=yaw_x(170*40:30065,1).*velocitycal(170*40:30065,1);
velocity_caly=yaw_y(170*40:30065,1).*velocitycal(170*40:30065,1);
north_estcal=cumtrapz(time(170*40:30065,1),velocity_calx);
east_estcal=cumtrapz(time(170*40:30065,1),velocity_caly);

figure (22)
% plot(east,north,'.'); hold on;
plot(east,north);hold on;
plot(east_est,north_est); hold on;

axis equal,
grid on,
title('Dead reckoning')
xlabel('north')
ylabel('east')
legend('north and east data from GPS', 'north and east data estimated from yaw and velocity')
hold off;

figure (25)
plot(east(170:752,1),north(170:752,1));hold on;
plot(east_estcal,north_estcal); hold on;

axis equal,
grid on,
title('Dead reckoning')
xlabel('north')
ylabel('east')
legend('north and east data from GPS', 'north and east data estimated from yaw and velocity')
hold off;
%allan deviation