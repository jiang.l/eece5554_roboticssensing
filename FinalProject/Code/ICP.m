num = 100;
% way too slow to debug with all of pcs

%% get transforms

sz = size(pc1,2);
tf = cell(1,sz-1);
cumtf = cell(1,sz-1);
for i=1:num-1
    tf{i} = pcregistericp(pc1{i+1},pc1{i});
    t = eye(4);
    for j=1:i
        t = t*tf{j}.T;
    end
    cumtf{i} = rigid3d(t);
end

%% transform data

tfpc1 = cell(1,sz);
tfpc1{1} = pc1{1};
for i=1:num-1
    tfpc1{i+1} = pctransform(pc1{i+1},cumtf{i});
end

%% plot results

figure(1)
pcshow(tfpc1{1});
hold on;
for i=2:num
    pcshow(tfpc1{i});
end

figure(2)
pcshow(pc1{1});
hold on;
for i=2:num
    pcshow(pc1{i});
end