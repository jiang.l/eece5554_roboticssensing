clc;
clear all;
close all;
format long

%% Read Data from files 
%Assign variables to evaluate
%msfile = 'C:/Users/music/Downloads/2017-10-18-17-33-13_0.bag';
msfile = 'C:\Users\music\Downloads\morning_stereo_rgb_ir_lidar_gps.bag';
msbag = rosbag(msfile);

%rosbag info 'C:/Users/music/Downloads/2017-10-18-17-33-13_0.bag'
rosbag info 'C:\Users\music\Downloads\morning_stereo_rgb_ir_lidar_gps.bag'

%% Read and assign relevant data
bag_info = rosbag('info', msfile);
%topics = msbag.Topics;

topic_ns1 = select(msbag, 'Topic', '/ns1/velodyne_points');
topic_ns2 = select(msbag, 'Topic', '/ns2/velodyne_points');
% ns1_pc = readMessages(topic_ns1, 1:2000);
% ns1_pc = [ns1_pc; readMessages(topic_ns1, 2001:4000)];
% ns1_pc = [ns1_pc; readMessages(topic_ns1, 4001:height(topic_ns1.MessageList))];
% ns2_pc = readMessages(topic_ns2, 1:2000);
% ns2_pc = [ns2_pc; readMessages(topic_ns2, 2001:4000)];
% ns2_pc = [ns2_pc; readMessages(topic_ns2, 4001:height(topic_ns2.MessageList))];
% ns1_data = cellfun(@(i) i.Data, ns1_pc, 'UniformOutput', 0);
% ns2_data = cellfun(@(i) i.Data, ns2_pc, 'UniformOutput', 0);
% ns1_fields = cellfun(@(i) i.Fields, ns1_pc, 'UniformOutput', 0);
% ns2_fields = cellfun(@(i) i.Fields, ns2_pc, 'UniformOutput', 0);

pc1={};
pc2={};

for i=1:height(topic_ns2.MessageList)
    
    temp_pc1 = readMessages(topic_ns1, i);
    pc1{i} = pointCloud(readXYZ(temp_pc1{1,1}), 'Intensity', readField(temp_pc1{1,1}, 'intensity'));
    temp_pc2 = readMessages(topic_ns2, i);
    pc2{i} = pointCloud(readXYZ(temp_pc2{1,1}), 'Intensity', readField(temp_pc2{1,1}, 'intensity'));

end

%% ICP algorithm

pcshow(pc1{1,1});
pcshow(pc2{1,1});

for i=1:height(topic_ns2.MessageList)
    file_name = ['pc1_', num2str(i), '.ply'];
    pcwrite(pc1{i}, file_name);
end

